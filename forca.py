import random

def jogar():
    imprimeMensagemAbertura()
    palavraSecreta = carregaPalavraSecreta()
    letrasAcertadas = inicializaLetrasAcertadas(palavraSecreta);

    enforcou = False
    acertou = False
    erros = 0;

    print(letrasAcertadas);

    while(not enforcou and not acertou):
        print("\n");

        chute = pedeChute()

        if(chute in palavraSecreta):
            marcaChuteCorreto(chute, letrasAcertadas, palavraSecreta);
        else:
            erros = erros+1;
            desenhaForca(erros);

        enforcou = erros == 7;
        acertou = "_" not in letrasAcertadas
        print(letrasAcertadas)

    imprimeMensagemFechamento(acertou, palavraSecreta)

def imprimeMensagemAbertura():
    print("\n******************************")
    print("Bem vindo ao jogo de Forca!");
    print("******************************\n")

def carregaPalavraSecreta():
    arquivo = open("palavras.txt", "r")
    palavras = [];
    for linha in arquivo:
        palavras.append(linha.strip())
    
    arquivo.close();

    palavraSecreta = palavras[random.randrange(0, len(palavras))].upper();

    return palavraSecreta

def inicializaLetrasAcertadas(palavraSecreta):
    return ["_" for letras in palavraSecreta];


def pedeChute():
    chute = input("Qual a letra? ")
    chute = chute.strip().upper()
    return chute

def marcaChuteCorreto(chute, letrasAcertadas, palavraSecreta):
    index = 0
    for letra in palavraSecreta:
        if(chute == letra):
            print("Encontrou letra {} na posição {}".format(letra, index))
            letrasAcertadas[index] = letra
        index = index + 1;

def imprimeMensagemFechamento(acertou, palavraSecreta):
    if(acertou):
        print("\nParabéns, você ganhou!")
        print("       ___________      ")
        print("      '._==_==_=_.'     ")
        print("      .-\\:      /-.    ")
        print("     | (|:.     |) |    ")
        print("      '-|:.     |-'     ")
        print("        \\::.    /      ")
        print("         '::. .'        ")
        print("           ) (          ")
        print("         _.' '._        ")
        print("        '-------'       ")
    else:
        print("\nPuxa, você foi enforcado!")
        print("A palavra era {}".format(palavraSecreta))
        print("    _______________         ")
        print("   /               \       ")
        print("  /                 \      ")
        print("//                   \/\  ")
        print("\|   XXXX     XXXX   | /   ")
        print(" |   XXXX     XXXX   |/     ")
        print(" |   XXX       XXX   |      ")
        print(" |                   |      ")
        print(" \__      XXX      __/     ")
        print("   |\     XXX     /|       ")
        print("   | |           | |        ")
        print("   | I I I I I I I |        ")
        print("   |  I I I I I I  |        ")
        print("   \_             _/       ")
        print("     \_         _/         ")
        print("       \_______/           ")

def desenhaForca(erros):
    print("  _______     ")
    print(" |/      |    ")

    if(erros == 1):
        print(" |      (_)   ")
        print(" |            ")
        print(" |            ")
        print(" |            ")

    if(erros == 2):
        print(" |      (_)   ")
        print(" |      \     ")
        print(" |            ")
        print(" |            ")

    if(erros == 3):
        print(" |      (_)   ")
        print(" |      \|    ")
        print(" |            ")
        print(" |            ")

    if(erros == 4):
        print(" |      (_)   ")
        print(" |      \|/   ")
        print(" |            ")
        print(" |            ")

    if(erros == 5):
        print(" |      (_)   ")
        print(" |      \|/   ")
        print(" |       |    ")
        print(" |            ")

    if(erros == 6):
        print(" |      (_)   ")
        print(" |      \|/   ")
        print(" |       |    ")
        print(" |      /     ")

    if (erros == 7):
        print(" |      (_)   ")
        print(" |      \|/   ")
        print(" |       |    ")
        print(" |      / \   ")

    print(" |            ")
    print("_|___         ")
    print()

if(__name__ == "__main__"):
    jogar();