import random

def jogar():
    print("\n******************************")
    print("Bem vindo ao jogo de Adivinha!");
    print("******************************\n")

    numeroSecreto = random.randrange(1, 101);
    totalDeTentativas = 0;
    pontos = 1000;

    print("Qual o nivel de dificuldade?");
    print("(1) Fácil (2) Médio (3) Difícil");

    nivel = int(input("Defina o nivel: "))

    if(nivel == 1):
        totalDeTentativas = 20;
    elif(nivel == 2):
        totalDeTentativas = 10;
    elif(nivel == 3):
        totalDeTentativas = 5;

    print("");

    for rodada in range(1, totalDeTentativas+1):
        print("Tentativa {} de {}".format(rodada, totalDeTentativas))

        chuteStr = input("Digite um numero entre 1 e 100: ");
        chuteInt = int(chuteStr);

        if(chuteInt < 1 or chuteInt > 100):
            print("Voce deve digitar um numero entre 1 e 100\n");
            continue;

        acertou = chuteInt == numeroSecreto;
        maior   = chuteInt > numeroSecreto
        menor   = chuteInt < numeroSecreto

        if(acertou):
            print("Voce acertou e fez {} pontos!\n".format(pontos))
            break;
        else:
            if(maior):
                print("Voce errou! Chute maior que o numero sercreto\n");
            elif(menor):
                print("Voce errou! Chute menor que o numero sercreto\n");
            
            pontosPerdinos = abs(numeroSecreto - chuteInt) 
            pontos = pontos - pontosPerdinos

    print("Fim do Jogo")

if(__name__ == "__main__"):
    jogar();